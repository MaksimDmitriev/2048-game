package ru.maksim.game2048.lib.interactor

import ru.maksim.game2048.lib.data.GameBoardConstants

interface SwipeHandler {

    fun handleSwipeLeft(input: IntArray)

    fun handleSwipeRight(input: IntArray)

    fun handleSwipeUp(input: IntArray)

    fun handleSwipeDown(input: IntArray)

}

internal class SwipeHandlerImpl : SwipeHandler {

    override fun handleSwipeLeft(input: IntArray) {
        for (i in 0 until GameBoardConstants.GAME_BOARD_SIZE step GameBoardConstants.GAME_BOARD_DIMENSION) {
            var leftElementIndex = i
            var currentElementIndex = leftElementIndex + 1
            val lastElementIndex = i + GameBoardConstants.GAME_BOARD_DIMENSION - 1

            while (currentElementIndex <= lastElementIndex) {
                if (input[currentElementIndex] == 0) {
                    currentElementIndex++
                    continue
                }

                when {
                    input[currentElementIndex] == input[leftElementIndex] -> {
                        input[leftElementIndex] = input[leftElementIndex] * 2
                        input[currentElementIndex] = 0
                        leftElementIndex++
                    }
                    input[leftElementIndex] == 0 -> {
                        input[leftElementIndex] = input[currentElementIndex]
                        input[currentElementIndex] = 0
                    }
                    else -> {
                        leftElementIndex++
                        if (input[leftElementIndex] == 0) {
                            input[leftElementIndex] = input[currentElementIndex]
                            input[currentElementIndex] = 0
                        }
                    }
                }
                currentElementIndex++
            }
        }
    }

    override fun handleSwipeRight(input: IntArray) {
        for (i in 0 until GameBoardConstants.GAME_BOARD_SIZE step GameBoardConstants.GAME_BOARD_DIMENSION) {
            var rightElementIndex = i + GameBoardConstants.GAME_BOARD_DIMENSION - 1
            var currentElementIndex = rightElementIndex - 1

            while (currentElementIndex >= i) {
                if (input[currentElementIndex] == 0) {
                    currentElementIndex--
                    continue
                }

                when {
                    input[currentElementIndex] == input[rightElementIndex] -> {
                        input[rightElementIndex] = input[rightElementIndex] * 2
                        input[currentElementIndex] = 0
                        rightElementIndex--
                    }
                    input[rightElementIndex] == 0 -> {
                        input[rightElementIndex] = input[currentElementIndex]
                        input[currentElementIndex] = 0
                    }
                    else -> {
                        rightElementIndex--
                        if (input[rightElementIndex] == 0) {
                            input[rightElementIndex] = input[currentElementIndex]
                            input[currentElementIndex] = 0
                        }
                    }
                }
                currentElementIndex--
            }
        }
    }

    override fun handleSwipeDown(input: IntArray) {
        for (i in 0 until GameBoardConstants.GAME_BOARD_DIMENSION) {
            var bottomElementIndex = i + GameBoardConstants.GAME_BOARD_SIZE - GameBoardConstants.GAME_BOARD_DIMENSION
            var currentElementIndex = bottomElementIndex - GameBoardConstants.GAME_BOARD_DIMENSION

            while (currentElementIndex >= i) {
                if (input[currentElementIndex] == 0) {
                    currentElementIndex -= GameBoardConstants.GAME_BOARD_DIMENSION
                    continue
                }

                when {
                    input[currentElementIndex] == input[bottomElementIndex] -> {
                        input[bottomElementIndex] = input[bottomElementIndex] * 2
                        input[currentElementIndex] = 0
                        bottomElementIndex -= GameBoardConstants.GAME_BOARD_DIMENSION
                    }
                    input[bottomElementIndex] == 0 -> {
                        input[bottomElementIndex] = input[currentElementIndex]
                        input[currentElementIndex] = 0
                    }
                    else -> {
                        bottomElementIndex -= GameBoardConstants.GAME_BOARD_DIMENSION
                        if (input[bottomElementIndex] == 0) {
                            input[bottomElementIndex] = input[currentElementIndex]
                            input[currentElementIndex] = 0
                        }
                    }
                }
                currentElementIndex -= GameBoardConstants.GAME_BOARD_DIMENSION
            }
        }
    }

    override fun handleSwipeUp(input: IntArray) {
        for (i in 0 until GameBoardConstants.GAME_BOARD_DIMENSION) {
            var topElementIndex = i
            var currentElementIndex = i + GameBoardConstants.GAME_BOARD_DIMENSION
            val lastElementIndex = i + GameBoardConstants.GAME_BOARD_SIZE - GameBoardConstants.GAME_BOARD_DIMENSION

            while (currentElementIndex <= lastElementIndex) {
                if (input[currentElementIndex] == 0) {
                    currentElementIndex += GameBoardConstants.GAME_BOARD_DIMENSION
                    continue
                }

                when {
                    input[currentElementIndex] == input[topElementIndex] -> {
                        input[topElementIndex] = input[topElementIndex] * 2
                        input[currentElementIndex] = 0
                        topElementIndex += GameBoardConstants.GAME_BOARD_DIMENSION
                    }
                    input[topElementIndex] == 0 -> {
                        input[topElementIndex] = input[currentElementIndex]
                        input[currentElementIndex] = 0
                    }
                    else -> {
                        topElementIndex += GameBoardConstants.GAME_BOARD_DIMENSION
                        if (input[topElementIndex] == 0) {
                            input[topElementIndex] = input[currentElementIndex]
                            input[currentElementIndex] = 0
                        }
                    }
                }
                currentElementIndex += GameBoardConstants.GAME_BOARD_DIMENSION
            }
        }
    }
}