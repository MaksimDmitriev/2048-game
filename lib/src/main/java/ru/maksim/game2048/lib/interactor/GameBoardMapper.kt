package ru.maksim.game2048.lib.interactor

import ru.maksim.game2048.lib.data.GameBoardConstants

interface GameBoardMapper {

    fun toPreference(data: IntArray): String

    fun toGameBoard(preference: String): IntArray
}

internal class GameBoardMapperImpl : GameBoardMapper {

    override fun toPreference(data: IntArray): String {
        val builder = StringBuilder()
        val lastIndex = data.lastIndex
        data.forEachIndexed { index, value ->
            if (index == lastIndex) {
                builder.append(value)
            } else {
                builder.append(value)
                    .append(GameBoardConstants.COMMA)
            }
        }
        return builder.toString()
    }

    override fun toGameBoard(preference: String): IntArray {
        val strings = preference.split(GameBoardConstants.COMMA)
        return IntArray(size = strings.size) { strings[it].toInt() }
    }

}