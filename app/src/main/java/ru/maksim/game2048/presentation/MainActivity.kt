package ru.maksim.game2048.presentation

import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GestureDetectorCompat
import dagger.hilt.android.AndroidEntryPoint
import ru.maksim.game2048.R
import ru.maksim.game2048.presentation.viewmodel.GamePlayViewModel

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var gestureDetectorCompat: GestureDetectorCompat
    private val gamePlayViewModel by viewModels<GamePlayViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        findViewById<View>(R.id.ic_outline_help_24).setOnClickListener {
            supportFragmentManager.beginTransaction()
                .add(HelpDialog.newInstance(), null)
                .commit()

        }
        gestureDetectorCompat = GestureDetectorCompat(this, GameBoardOnGestureListener(gamePlayViewModel))
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        gestureDetectorCompat.onTouchEvent(event)
        return super.onTouchEvent(event)
    }
}