package ru.maksim.game2048.lib.interactor.random

import androidx.annotation.VisibleForTesting
import ru.maksim.game2048.lib.data.GameBoardConstants
import ru.maksim.game2048.lib.interactor.GameOverCheck

interface GameBoardValuesGenerator {

    fun generateNewGameBoard(): IntArray

    fun addValueToGameBoard(gameBoard: IntArray)
}

internal class GameBoardValuesGeneratorImpl(
    private val randomNumberGenerator: RandomNumberGenerator
) : GameBoardValuesGenerator {

    override fun generateNewGameBoard(): IntArray {
        val indexes = IntArray(size = GameBoardConstants.GAME_BOARD_SIZE) { it }
        indexes.shuffle()
        val value = generateNewValue()
        return IntArray(size = GameBoardConstants.GAME_BOARD_SIZE) { index ->
            when (index) {
                indexes[0], indexes[1] -> value
                else -> 0
            }
        }
    }

    override fun addValueToGameBoard(gameBoard: IntArray) {
        val index = generateNextValueIndex(gameBoard)
        if (index == -1) {
            return
        }
        val value = generateNewValue()
        gameBoard[index] = value
    }

    @VisibleForTesting
    fun generateNextValueIndex(gameBoard: IntArray): Int {
        val emptyIndexes = mutableListOf<Int>()
        gameBoard.forEachIndexed { index, elem ->
            if (elem == 0) {
                emptyIndexes.add(index)
            } else if (elem == GameOverCheck.WINNING_NUMBER) {
                return -1
            }
        }
        emptyIndexes.shuffle()
        return if (emptyIndexes.size == 0) -1 else emptyIndexes[0]
    }

    @VisibleForTesting
    fun generateNewValue(): Int {
        return if (randomNumberGenerator.nextInt() % 2 == 0) {
            2
        } else {
            4
        }
    }
}