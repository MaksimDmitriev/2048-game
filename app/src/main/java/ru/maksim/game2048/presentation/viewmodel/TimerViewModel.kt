package ru.maksim.game2048.presentation.viewmodel

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.shareIn
import kotlinx.coroutines.launch
import ru.maksim.game2048.lib.data.AppConstants
import ru.maksim.game2048.lib.data.preferences.Repository
import ru.maksim.game2048.lib.domain.GamePhase
import ru.maksim.game2048.lib.framework.AppCoroutineScope
import ru.maksim.game2048.lib.interactor.TimeFormatter
import javax.inject.Inject

@HiltViewModel
class TimerViewModel @Inject constructor(
    private val timeFormatter: TimeFormatter,
    private val repository: Repository,
    private val appCoroutineScope: AppCoroutineScope
) : ViewModel() {

    private var _timeElapsed = 0L
    private val gamePhaseFlow = repository.observeGamePhase()
        .distinctUntilChanged()
        .shareIn(
            scope = viewModelScope,
            started = SharingStarted.Lazily,
            replay = 2
        )

    fun observeTimer() =
        gamePhaseFlow
            .flatMapLatest { phase ->
                when (phase) {
                    GamePhase.NOT_STARTED -> {
                        flowOfZero()
                            .onCompletion {
                                resetTimeElapsed()
                            }
                    }
                    GamePhase.IN_PROGRESS -> {
                        observeTimeElapsed()
                            .map { millis -> timeFormatter.format(millis) }
                    }
                    GamePhase.WON -> {
                        flowOfTimeElapsed()
                            .onCompletion {
                                if (HAS_JUST_WON_PHASES == gamePhaseFlow.replayCache) {
                                    updateBestTime()
                                }
                            }
                    }
                    else -> flowOfTimeElapsed()
                }
            }

    private fun flowOfTimeElapsed() = flowOf(_timeElapsed)
        .map { millis -> timeFormatter.format(millis) }

    private fun flowOfZero() = flowOf(0L)
        .map { millis -> timeFormatter.format(millis) }

    private suspend fun updateBestTime() {
        val bestTime = repository.observeBestTime().first()
        Log.d(AppConstants.TAG, "best time: $bestTime _timeElapsed: $_timeElapsed")
        if (_timeElapsed < bestTime) {
            viewModelScope.launch {
                repository.saveBestTime(_timeElapsed)
            }
        }
    }

    private suspend fun resetTimeElapsed() {
        _timeElapsed = 0L
        repository.saveTimeElapsed(0L)
        Log.d(AppConstants.TAG, "resetTimeElapsed _timeElapsed: $_timeElapsed")
    }

    private fun observeTimeElapsed() = flow {
        val startTime = System.currentTimeMillis()
        val timeElapsedAtStart = repository.observeTimeElapsed().first()
        while (true) {
            _timeElapsed = timeElapsedAtStart + System.currentTimeMillis() - startTime
            emit(_timeElapsed)
            Log.d(AppConstants.TAG, "_timeElapsed: $_timeElapsed")
            delay(1_000L)
        }
    }

    fun saveTimeElapsed() {
        appCoroutineScope.scope.launch {
            // TODO: Can this lead to a memory leak? I use this view model instance to run an operation
            // that can outlive the view model
            repository.saveTimeElapsed(_timeElapsed)
        }
    }

    companion object {
        private val HAS_JUST_WON_PHASES = listOf(GamePhase.IN_PROGRESS, GamePhase.WON)
    }
}