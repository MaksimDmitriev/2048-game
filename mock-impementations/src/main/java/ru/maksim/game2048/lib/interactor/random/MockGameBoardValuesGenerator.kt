package ru.maksim.game2048.lib.interactor.random

import ru.maksim.game2048.lib.data.GameBoardConstants

class MockGameBoardValuesGenerator : GameBoardValuesGenerator {

    var gameBoard: IntArray = IntArray(size = GameBoardConstants.GAME_BOARD_SIZE) { 0 }
    var indexToAddNewValue = 0
    var newValue = 2

    override fun generateNewGameBoard() = gameBoard

    override fun addValueToGameBoard(gameBoard: IntArray) {
        gameBoard[indexToAddNewValue] = newValue
    }

}