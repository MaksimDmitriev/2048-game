package ru.maksim.game2048.lib.interactor

import java.util.Locale
import java.util.concurrent.TimeUnit

interface TimeFormatter {

    fun format(millis: Long): String
}

internal class TimeFormatterImpl : TimeFormatter {

    override fun format(millis: Long): String {
        val hours = millis / HOUR_IN_MILLIS
        val hourRemainder = millis % HOUR_IN_MILLIS
        val minutes = hourRemainder / MINUTE_IN_MILLIS
        val minutesRemainder = hourRemainder % MINUTE_IN_MILLIS
        val seconds = minutesRemainder / SECOND_IN_MILLIS
        return String.format(Locale.ENGLISH, "%02d:%02d:%02d", hours, minutes, seconds)
    }

    companion object {
        private val HOUR_IN_MILLIS = TimeUnit.HOURS.toMillis(1L)
        private val MINUTE_IN_MILLIS = TimeUnit.MINUTES.toMillis(1L)
        private val SECOND_IN_MILLIS = TimeUnit.SECONDS.toMillis(1L)
    }

}