package ru.maksim.game2048.presentation

import android.content.Context
import android.graphics.Rect
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ItemDecoration
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import ru.maksim.game2048.R
import ru.maksim.game2048.lib.data.GameBoardConstants
import ru.maksim.game2048.lib.domain.GamePhase
import ru.maksim.game2048.presentation.viewmodel.GamePlayViewModel

@AndroidEntryPoint
class GameFragment : Fragment(R.layout.fragment_game) {

    private val gamePlayViewModel by activityViewModels<GamePlayViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val adapter = GameAdapter(IntArray(size = GameBoardConstants.GAME_BOARD_SIZE) { 0 }, requireContext())
        val layoutManager = GridLayoutManager(context, GameBoardConstants.GAME_BOARD_DIMENSION)
        val itemSpace = resources.getDimensionPixelSize(R.dimen.game_cell_gap)
        val dividerItemDecoration = SpacesItemDecoration(itemSpace)
        val gameBoardRecyclerView = view.findViewById<RecyclerView>(R.id.game_board)
        gameBoardRecyclerView.addItemDecoration(dividerItemDecoration)
        gameBoardRecyclerView.layoutManager = layoutManager
        gameBoardRecyclerView.adapter = adapter

        val gameOverTextView = view.findViewById<TextView>(R.id.game_over)

        // Create a new coroutine in the lifecycleScope
        viewLifecycleOwner.lifecycleScope.launch {
            // repeatOnLifecycle launches the block in a new coroutine every time the
            // lifecycle is in the STARTED state (or above) and cancels it when it's STOPPED.
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                // Trigger the flow and start listening for values.
                // This happens when lifecycle is STARTED and stops
                // collecting when the lifecycle is STOPPED
                gamePlayViewModel.myUiState
                    .collect {
                        adapter.setNewData(it.gameBoard)
                        adapter.notifyItemRangeChanged(0, GameBoardConstants.GAME_BOARD_SIZE)
                        setGameOverMessage(it.gamePhase, gameOverTextView)
                    }
            }
        }
    }

    private fun setGameOverMessage(@GamePhase phase: Int, gameOverTextView: TextView) {
        when (phase) {
            GamePhase.NOT_STARTED -> {
                gameOverTextView.visibility = View.GONE
                gameOverTextView.text = GameBoardConstants.EMPTY_STRING
            }
            GamePhase.WON -> {
                gameOverTextView.visibility = View.VISIBLE
                gameOverTextView.setText(R.string.you_won)
            }
            GamePhase.LOST -> {
                gameOverTextView.visibility = View.VISIBLE
                gameOverTextView.setText(R.string.you_lost)
            }
        }
    }
}

class GameAdapter(private var data: IntArray, private val context: Context) :
    RecyclerView.Adapter<GameAdapter.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val gameCellTextView: TextView = view as TextView
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.layout_game_cell, viewGroup, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val num = data[position]
        if (num == 0) {
            viewHolder.gameCellTextView.text = GameBoardConstants.EMPTY_STRING
        } else {
            viewHolder.gameCellTextView.text = num.toString()
        }
        viewHolder.gameCellTextView.setBackgroundColor(ContextCompat.getColor(context, NumberToColorMap.getSafely(num)))
    }

    override fun getItemCount() = data.size

    fun setNewData(newData: IntArray) {
        data = newData
    }
}

private class SpacesItemDecoration(private val space: Int) : ItemDecoration() {

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        outRect.left = space
        outRect.right = space
        outRect.bottom = space
        outRect.top = space
    }
}

object NumberToColorMap {

    private val map = mapOf(
        2 to R.color.color_2,
        4 to R.color.color_4,
        8 to R.color.color_8,
        16 to R.color.color_16,
        32 to R.color.color_32,
        64 to R.color.color_64,
        128 to R.color.color_128,
        256 to R.color.color_256,
        512 to R.color.color_512,
        1024 to R.color.color_1024,
        2048 to R.color.color_2048
    )

    @ColorRes
    fun getSafely(num: Int) = map[num] ?: R.color.color_default
}