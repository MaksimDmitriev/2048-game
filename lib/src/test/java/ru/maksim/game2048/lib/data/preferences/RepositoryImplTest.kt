package ru.maksim.game2048.lib.data.preferences

import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito

internal class RepositoryImplTest {

    private lateinit var repositoryImpl: RepositoryImpl
    private lateinit var dataSource: DataSource

    @BeforeEach
    fun setUp() {
        dataSource = Mockito.mock(DataSource::class.java)
        repositoryImpl = RepositoryImpl(dataSource)
    }

    @Test
    fun observeBestTime() {
        val value = 1L
        Mockito.`when`(dataSource.observeBestTime())
            .thenReturn(flow { emit(value) })

        runBlocking {
            val actual = repositoryImpl.observeBestTime().first()
            Assertions.assertEquals(value, actual)
        }
    }

    @Test
    fun saveBestTime() {
        val value = 1L
        runBlocking {
            repositoryImpl.saveBestTime(value)
            Mockito.verify(dataSource).saveBestTime(value)
            Mockito.verifyNoMoreInteractions(dataSource)
        }
    }

    @Test
    fun observeGameBoard() {
        val value = "1"
        Mockito.`when`(dataSource.observeGameBoard())
            .thenReturn(flow { emit(value) })

        runBlocking {
            val actual = repositoryImpl.observeGameBoard().first()
            Assertions.assertEquals(value, actual)
        }
    }

    @Test
    fun saveGameBoard() {
        val value = "1,2,3"
        runBlocking {
            repositoryImpl.saveGameBoard(value)
            Mockito.verify(dataSource).saveGameBoard(value)
            Mockito.verifyNoMoreInteractions(dataSource)
        }
    }

    @Test
    fun observeGamePhase() {

    }

    @Test
    fun saveGamePhase() {

    }
}
