package ru.maksim.game2048.lib.data.preferences

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.mockito.Mockito

internal class LocalDataSourceTest {

    private lateinit var localDataSource: LocalDataSource

    @Test
    fun observeBestTime() {
        // prepare
        val preferencesDataStore = MockPreferencesDataStore()
        val dataStore = Mockito.mock(DataStore::class.java) as DataStore<Preferences>
        val preferences = Mockito.mock(Preferences::class.java)
        val bestTimeVal = 1_000L
        Mockito.`when`(preferences[PreferencesKeys.BEST_TIME])
            .thenReturn(bestTimeVal)
        val data = flow<Preferences> {
            emit(preferences)
        }
        Mockito.`when`(dataStore.data)
            .thenReturn(data)

        preferencesDataStore.dataStoreInstance = dataStore

        localDataSource = LocalDataSource(preferencesDataStore)

        // execute
        runBlocking {
            val res = localDataSource.observeBestTime()
                .first()
            Assertions.assertEquals(bestTimeVal, res)
        }
    }

    @Test
    fun `observeBestTime exception`() {
        // prepare
        val preferencesDataStore = MockPreferencesDataStore()
        val dataStore = Mockito.mock(DataStore::class.java) as DataStore<Preferences>
        val data = flow<Preferences> {
            throw RuntimeException()
        }
        Mockito.`when`(dataStore.data)
            .thenReturn(data)

        preferencesDataStore.dataStoreInstance = dataStore

        localDataSource = LocalDataSource(preferencesDataStore)

        // execute
        runBlocking {
            val res = localDataSource.observeBestTime().first()
            Assertions.assertEquals(Long.MAX_VALUE, res)
        }
    }

    @Disabled
    @Test
    fun saveBestTime() {
        // prepare
        val preferencesDataStore = MockPreferencesDataStore()
        val dataStore = Mockito.mock(DataStore::class.java) as DataStore<Preferences>
        preferencesDataStore.dataStoreInstance = dataStore

        localDataSource = LocalDataSource(preferencesDataStore)

        val bestTimeVal = 1_000L
        runBlocking {
            localDataSource.saveBestTime(bestTimeVal)
            // TODO: ask how to test such case
            Mockito.verify(dataStore).edit { Mockito.any() }
        }
    }

    @Test
    fun observeGameBoard() {
        // prepare
        val preferencesDataStore = MockPreferencesDataStore()
        val dataStore = Mockito.mock(DataStore::class.java) as DataStore<Preferences>
        val preferences = Mockito.mock(Preferences::class.java)
        val gameBoard = "1,2,3"
        Mockito.`when`(preferences[PreferencesKeys.GAME_BOARD_DATA])
            .thenReturn(gameBoard)
        val data = flow<Preferences> {
            emit(preferences)
        }
        Mockito.`when`(dataStore.data)
            .thenReturn(data)

        preferencesDataStore.dataStoreInstance = dataStore

        localDataSource = LocalDataSource(preferencesDataStore)

        // execute
        runBlocking {
            val res = localDataSource.observeGameBoard().first()
            Assertions.assertEquals(gameBoard, res)
        }
    }

    @Test
    fun `observeGameBoard exception`() {
    }

    @Test
    fun saveGameBoard() {

    }

    @Test
    fun saveGamePhase() {

    }

    @Test
    fun observeGamePhase() {

    }

    @Test
    fun `observeGamePhase exception`() {

    }
}