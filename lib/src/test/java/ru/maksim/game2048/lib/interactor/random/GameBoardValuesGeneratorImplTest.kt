package ru.maksim.game2048.lib.interactor.random

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.mockito.Mockito

class GameBoardValuesGeneratorImplTest {

    @Test
    fun `generateNewValue 2`() {
        val randomNumberGenerator = Mockito.mock(RandomNumberGenerator::class.java)
        val gameBoardValuesGeneratorImpl = GameBoardValuesGeneratorImpl(randomNumberGenerator)

        Mockito.`when`(randomNumberGenerator.nextInt())
            .thenReturn(16)

        Assertions.assertEquals(2, gameBoardValuesGeneratorImpl.generateNewValue())
    }

    @Test
    fun `generateNewValue 4`() {
        val randomNumberGenerator = Mockito.mock(RandomNumberGenerator::class.java)
        val gameBoardValuesGeneratorImpl = GameBoardValuesGeneratorImpl(randomNumberGenerator)

        Mockito.`when`(randomNumberGenerator.nextInt())
            .thenReturn(5)

        Assertions.assertEquals(4, gameBoardValuesGeneratorImpl.generateNewValue())
    }

    @Test
    fun generateNewGameBoard() {

    }

    @Test
    fun addValueToGameBoard() {}

    @Test
    fun generateNextValueIndex() {}
}