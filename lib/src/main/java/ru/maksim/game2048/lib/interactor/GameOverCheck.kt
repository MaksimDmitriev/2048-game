package ru.maksim.game2048.lib.interactor

import ru.maksim.game2048.lib.data.GameBoardConstants

interface GameOverCheck {

    fun hasWon(input: IntArray): Boolean

    fun hasLost(input: IntArray): Boolean

    companion object {
        const val WINNING_NUMBER = 2048
    }
}

internal class GameOverCheckImpl : GameOverCheck {

    override fun hasWon(input: IntArray): Boolean {
        input.forEach {
            if (it == GameOverCheck.WINNING_NUMBER) {
                return true
            }
        }
        return false
    }

    override fun hasLost(input: IntArray): Boolean {
        val edgeIndex = GameBoardConstants.GAME_BOARD_DIMENSION - 1
        for (i in 0 until GameBoardConstants.GAME_BOARD_SIZE) {
            if (input[i] == 0) {
                return false
            }
            val hasElementToRight = i % GameBoardConstants.GAME_BOARD_DIMENSION != edgeIndex
            val hasElementToBottom = i / GameBoardConstants.GAME_BOARD_DIMENSION != edgeIndex
            if (hasElementToRight && input[i] == input[i + 1]) {
                return false
            }
            if (hasElementToBottom && input[i] == input[i + GameBoardConstants.GAME_BOARD_DIMENSION]) {
                return false
            }
        }
        return true
    }

}
