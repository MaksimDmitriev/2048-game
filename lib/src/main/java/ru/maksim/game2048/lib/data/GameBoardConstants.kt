package ru.maksim.game2048.lib.data

object GameBoardConstants {
    const val GAME_BOARD_DIMENSION = 4
    const val GAME_BOARD_SIZE = GAME_BOARD_DIMENSION * GAME_BOARD_DIMENSION
    const val EMPTY_STRING = ""
    const val COMMA = ','
}
