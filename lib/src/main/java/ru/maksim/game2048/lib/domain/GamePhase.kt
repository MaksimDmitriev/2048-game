package ru.maksim.game2048.lib.domain

import androidx.annotation.IntDef

@IntDef(value = [GamePhase.NOT_STARTED, GamePhase.IN_PROGRESS, GamePhase.WON, GamePhase.LOST])
annotation class GamePhase {

    companion object {
        const val NOT_STARTED = 1
        const val IN_PROGRESS = 2
        const val WON = 3
        const val LOST = 4
    }
}