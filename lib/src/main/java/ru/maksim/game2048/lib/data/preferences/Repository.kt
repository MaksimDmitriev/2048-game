package ru.maksim.game2048.lib.data.preferences

import kotlinx.coroutines.flow.Flow
import ru.maksim.game2048.lib.domain.GamePhase

interface Repository {

    fun observeBestTime(): Flow<Long>

    suspend fun saveBestTime(bestTime: Long)

    fun observeGameBoard(): Flow<String>

    suspend fun saveGameBoard(gameBoardData: String)

    fun observeGamePhase(): Flow<Int>

    suspend fun saveGamePhase(@GamePhase phase: Int)

    suspend fun saveTimeElapsed(timeElapsed: Long)

    fun observeTimeElapsed(): Flow<Long>
}

internal class RepositoryImpl(private val dataSource: DataSource) : Repository {

    override fun observeBestTime() = dataSource.observeBestTime()

    override suspend fun saveBestTime(bestTime: Long) = dataSource.saveBestTime(bestTime)

    override fun observeGameBoard() = dataSource.observeGameBoard()

    override suspend fun saveGameBoard(gameBoardData: String) = dataSource.saveGameBoard(gameBoardData)

    override suspend fun saveGamePhase(phase: Int) = dataSource.saveGamePhase(phase)

    override fun observeGamePhase() = dataSource.observeGamePhase()

    override suspend fun saveTimeElapsed(timeElapsed: Long) = dataSource.saveTimeElapsed(timeElapsed)

    override fun observeTimeElapsed() = dataSource.observeTimeElapsed()
}