package ru.maksim.game2048.lib.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped
import ru.maksim.game2048.lib.interactor.random.GameBoardValuesGenerator
import ru.maksim.game2048.lib.interactor.random.GameBoardValuesGeneratorImpl
import ru.maksim.game2048.lib.interactor.random.RandomNumberGeneratorImpl

@Module
@InstallIn(ViewModelComponent::class)
object GameBoardValuesGeneratorModule {

    @Provides
    @ViewModelScoped
    fun provideGameBoardValuesGenerator(): GameBoardValuesGenerator = GameBoardValuesGeneratorImpl(RandomNumberGeneratorImpl())
}