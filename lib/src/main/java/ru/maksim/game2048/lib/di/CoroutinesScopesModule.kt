package ru.maksim.game2048.lib.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import ru.maksim.game2048.lib.framework.AppCoroutineScope
import ru.maksim.game2048.lib.framework.AppCoroutineScopeImpl
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object CoroutinesScopesModule {

    @Singleton
    @Provides
    fun providesCoroutineScope(): AppCoroutineScope = AppCoroutineScopeImpl()
}