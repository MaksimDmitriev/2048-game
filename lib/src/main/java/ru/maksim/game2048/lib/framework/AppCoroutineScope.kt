package ru.maksim.game2048.lib.framework

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob

interface AppCoroutineScope {

    val scope: CoroutineScope
}

internal class AppCoroutineScopeImpl : AppCoroutineScope {

    private val _scope = CoroutineScope(SupervisorJob() + Dispatchers.Default)

    override val scope: CoroutineScope
        get() = _scope
}