package ru.maksim.game2048.lib.interactor

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import java.util.concurrent.TimeUnit

internal class TimeFormatterImplTest {

    private val timeFormatterImpl = TimeFormatterImpl()

    @Test
    fun `format seconds`() {
        val millis = TimeUnit.SECONDS.toMillis(10)
        val res = timeFormatterImpl.format(millis)
        Assertions.assertEquals("00:00:10", res)
    }

    @Test
    fun `format minutes`() {
        val millis = TimeUnit.MINUTES.toMillis(10)
        val res = timeFormatterImpl.format(millis)
        Assertions.assertEquals("00:10:00", res)
    }

    @Test
    fun `format hours`() {
        val millis = TimeUnit.HOURS.toMillis(2)
        val res = timeFormatterImpl.format(millis)
        Assertions.assertEquals("02:00:00", res)
    }

    @Test
    fun `format hours and minutes`() {
        val millis = TimeUnit.HOURS.toMillis(2) + TimeUnit.MINUTES.toMillis(1)
        val res = timeFormatterImpl.format(millis)
        Assertions.assertEquals("02:01:00", res)
    }

    @Test
    fun `format hours and seconds`() {
        val millis = TimeUnit.HOURS.toMillis(2) + TimeUnit.SECONDS.toMillis(43)
        val res = timeFormatterImpl.format(millis)
        Assertions.assertEquals("02:00:43", res)
    }

    @Test
    fun `format hours, minutes and seconds`() {
        val millis = TimeUnit.HOURS.toMillis(2) + TimeUnit.MINUTES.toMillis(12) + TimeUnit.SECONDS.toMillis(43)
        val res = timeFormatterImpl.format(millis)
        Assertions.assertEquals("02:12:43", res)
    }

    @Test
    fun `format zeros`() {
        val res = timeFormatterImpl.format(0)
        Assertions.assertEquals("00:00:00", res)
    }

    @Test
    fun `format 59 minutes`() {
        val millis = TimeUnit.MINUTES.toMillis(59)
        val res = timeFormatterImpl.format(millis)
        Assertions.assertEquals("00:59:00", res)
    }

    @Test
    fun `format 59 seconds`() {
        val millis = TimeUnit.SECONDS.toMillis(59)
        val res = timeFormatterImpl.format(millis)
        Assertions.assertEquals("00:00:59", res)
    }
}

