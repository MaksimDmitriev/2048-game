package ru.maksim.game2048.lib.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped
import ru.maksim.game2048.lib.interactor.GameBoardMapper
import ru.maksim.game2048.lib.interactor.GameBoardMapperImpl
import ru.maksim.game2048.lib.interactor.GameOverCheck
import ru.maksim.game2048.lib.interactor.GameOverCheckImpl
import ru.maksim.game2048.lib.interactor.SwipeHandler
import ru.maksim.game2048.lib.interactor.SwipeHandlerImpl
import ru.maksim.game2048.lib.interactor.TimeFormatter
import ru.maksim.game2048.lib.interactor.TimeFormatterImpl

@Module
@InstallIn(ViewModelComponent::class)
internal object ViewModelModule {

    @Provides
    @ViewModelScoped
    fun provideGameBoardMapperImpl(): GameBoardMapper = GameBoardMapperImpl()

    @Provides
    @ViewModelScoped
    fun provideSwipeHandler(): SwipeHandler = SwipeHandlerImpl()

    @Provides
    @ViewModelScoped
    fun provideGameOverCheck(): GameOverCheck = GameOverCheckImpl()

    @Provides
    @ViewModelScoped
    fun provideTimeFormatter(): TimeFormatter = TimeFormatterImpl()
}