package ru.maksim.game2048.lib.interactor

import org.junit.jupiter.api.Assertions.assertArrayEquals
import org.junit.jupiter.api.Test
import ru.maksim.game2048.lib.interactor.SwipeHandlerImpl

internal class SwipeHandlerImplTest {

    private val swipeHandler = SwipeHandlerImpl()

    @Test
    fun `swipeLeft no merger, only shifts`() {
        val input = intArrayOf(
            0, 0, 0, 2,
            0, 0, 2, 0,
            0, 2, 0, 0,
            2, 0, 0, 0
        )
        val expected = intArrayOf(
            2, 0, 0, 0,
            2, 0, 0, 0,
            2, 0, 0, 0,
            2, 0, 0, 0
        )

        swipeHandler.handleSwipeLeft(input)
        assertArrayEquals(expected, input)
    }

    @Test
    fun `swipeLeft no merger, no shifts`() {
        val input = intArrayOf(
            2, 4, 2, 4,
            2, 4, 2, 4,
            2, 4, 2, 4,
            2, 4, 2, 4
        )
        val expected = intArrayOf(
            2, 4, 2, 4,
            2, 4, 2, 4,
            2, 4, 2, 4,
            2, 4, 2, 4
        )

        swipeHandler.handleSwipeLeft(input)
        assertArrayEquals(expected, input)
    }

    @Test
    fun `swipeLeft one merger`() {
        val input = intArrayOf(
            0, 2, 2, 0,
            2, 0, 2, 0,
            0, 2, 0, 2,
            2, 0, 0, 2
        )
        val expected = intArrayOf(
            4, 0, 0, 0,
            4, 0, 0, 0,
            4, 0, 0, 0,
            4, 0, 0, 0
        )

        swipeHandler.handleSwipeLeft(input)
        assertArrayEquals(expected, input)
    }

    @Test
    fun `swipeLeft two mergers`() {
        val input = intArrayOf(
            2, 2, 2, 2,
            0, 0, 0, 0,
            2, 2, 2, 2,
            0, 0, 0, 0
        )
        val expected = intArrayOf(
            4, 4, 0, 0,
            0, 0, 0, 0,
            4, 4, 0, 0,
            0, 0, 0, 0
        )

        swipeHandler.handleSwipeLeft(input)
        assertArrayEquals(expected, input)
    }

    @Test
    fun `swipeLeft three numbers and one merger`() {
        val input = intArrayOf(
            2, 0, 2, 2,
            2, 2, 2, 0,
            0, 2, 2, 2,
            2, 2, 0, 2
        )
        val expected = intArrayOf(
            4, 2, 0, 0,
            4, 2, 0, 0,
            4, 2, 0, 0,
            4, 2, 0, 0
        )

        swipeHandler.handleSwipeLeft(input)
        assertArrayEquals(expected, input)
    }

    @Test
    fun `swipeLeft 2 0 4 0`() {
        val input = intArrayOf(
            2, 0, 4, 0,
            2, 0, 4, 0,
            2, 0, 4, 0,
            2, 0, 4, 0
        )
        val expected = intArrayOf(
            2, 4, 0, 0,
            2, 4, 0, 0,
            2, 4, 0, 0,
            2, 4, 0, 0
        )

        swipeHandler.handleSwipeLeft(input)
        assertArrayEquals(expected, input)
    }

    @Test
    fun `swipeLeft 2 4 0 4`() {
        val input = intArrayOf(
            2, 4, 0, 4,
            2, 4, 0, 4,
            2, 4, 0, 4,
            2, 4, 0, 4
        )
        val expected = intArrayOf(
            2, 8, 0, 0,
            2, 8, 0, 0,
            2, 8, 0, 0,
            2, 8, 0, 0
        )

        swipeHandler.handleSwipeLeft(input)
        assertArrayEquals(expected, input)
    }

    @Test
    fun `swipeRight no merger, only shifts`() {
        val input = intArrayOf(
            0, 0, 0, 2,
            0, 0, 2, 0,
            0, 2, 0, 0,
            2, 0, 0, 0
        )
        val expected = intArrayOf(
            0, 0, 0, 2,
            0, 0, 0, 2,
            0, 0, 0, 2,
            0, 0, 0, 2
        )

        swipeHandler.handleSwipeRight(input)
        assertArrayEquals(expected, input)
    }

    @Test
    fun `swipeRight no merger, no shifts`() {
        val input = intArrayOf(
            2, 4, 2, 4,
            2, 4, 2, 4,
            2, 4, 2, 4,
            2, 4, 2, 4
        )
        val expected = intArrayOf(
            2, 4, 2, 4,
            2, 4, 2, 4,
            2, 4, 2, 4,
            2, 4, 2, 4
        )

        swipeHandler.handleSwipeRight(input)
        assertArrayEquals(expected, input)
    }

    @Test
    fun `swipeRight one merger`() {
        val input = intArrayOf(
            0, 2, 2, 0,
            2, 0, 2, 0,
            0, 2, 0, 2,
            2, 0, 0, 2
        )
        val expected = intArrayOf(
            0, 0, 0, 4,
            0, 0, 0, 4,
            0, 0, 0, 4,
            0, 0, 0, 4
        )

        swipeHandler.handleSwipeRight(input)
        assertArrayEquals(expected, input)
    }

    @Test
    fun `swipeRight two mergers`() {
        val input = intArrayOf(
            2, 2, 2, 2,
            0, 0, 0, 0,
            2, 2, 2, 2,
            0, 0, 0, 0
        )
        val expected = intArrayOf(
            0, 0, 4, 4,
            0, 0, 0, 0,
            0, 0, 4, 4,
            0, 0, 0, 0
        )

        swipeHandler.handleSwipeRight(input)
        assertArrayEquals(expected, input)
    }

    @Test
    fun `swipeRight three numbers and one merger`() {
        val input = intArrayOf(
            2, 0, 2, 2,
            2, 2, 2, 0,
            0, 2, 2, 2,
            2, 2, 0, 2
        )
        val expected = intArrayOf(
            0, 0, 2, 4,
            0, 0, 2, 4,
            0, 0, 2, 4,
            0, 0, 2, 4
        )

        swipeHandler.handleSwipeRight(input)
        assertArrayEquals(expected, input)
    }

    @Test
    fun `swipeRight 2 0 4 0`() {
        val input = intArrayOf(
            2, 0, 4, 0,
            2, 0, 4, 0,
            2, 0, 4, 0,
            2, 0, 4, 0
        )
        val expected = intArrayOf(
            0, 0, 2, 4,
            0, 0, 2, 4,
            0, 0, 2, 4,
            0, 0, 2, 4
        )

        swipeHandler.handleSwipeRight(input)
        assertArrayEquals(expected, input)
    }

    @Test
    fun `swipeRight 2 4 0 4`() {
        val input = intArrayOf(
            2, 4, 0, 4,
            2, 4, 0, 4,
            2, 4, 0, 4,
            2, 4, 0, 4
        )
        val expected = intArrayOf(
            0, 0, 2, 8,
            0, 0, 2, 8,
            0, 0, 2, 8,
            0, 0, 2, 8
        )

        swipeHandler.handleSwipeRight(input)
        assertArrayEquals(expected, input)
    }

    @Test
    fun `handleSwipeDown no merger, only shifts`() {
        val input = intArrayOf(
            0, 0, 0, 2,
            0, 0, 2, 0,
            0, 2, 0, 0,
            2, 0, 0, 0
        )
        val expected = intArrayOf(
            0, 0, 0, 0,
            0, 0, 0, 0,
            0, 0, 0, 0,
            2, 2, 2, 2
        )

        swipeHandler.handleSwipeDown(input)
        assertArrayEquals(expected, input)
    }

    @Test
    fun `handleSwipeDown no merger, no shifts`() {
        val input = intArrayOf(
            2, 2, 2, 2,
            4, 4, 4, 4,
            2, 2, 2, 2,
            4, 4, 4, 4
        )
        val expected = intArrayOf(
            2, 2, 2, 2,
            4, 4, 4, 4,
            2, 2, 2, 2,
            4, 4, 4, 4
        )

        swipeHandler.handleSwipeDown(input)
        assertArrayEquals(expected, input)
    }

    @Test
    fun `handleSwipeDown one merger`() {
        val input = intArrayOf(
            0, 0, 0, 0,
            2, 0, 2, 0,
            2, 0, 2, 0,
            0, 0, 0, 0
        )
        val expected = intArrayOf(
            0, 0, 0, 0,
            0, 0, 0, 0,
            0, 0, 0, 0,
            4, 0, 4, 0
        )

        swipeHandler.handleSwipeDown(input)
        assertArrayEquals(expected, input)
    }

    @Test
    fun `handleSwipeDown two mergers`() {
        val input = intArrayOf(
            2, 2, 2, 2,
            2, 2, 2, 2,
            2, 2, 2, 2,
            2, 2, 2, 2
        )
        val expected = intArrayOf(
            0, 0, 0, 0,
            0, 0, 0, 0,
            4, 4, 4, 4,
            4, 4, 4, 4
        )

        swipeHandler.handleSwipeDown(input)
        assertArrayEquals(expected, input)
    }

    @Test
    fun `handleSwipeDown three numbers and one merger`() {
        val input = intArrayOf(
            2, 2, 2, 2,
            0, 0, 0, 0,
            2, 2, 2, 2,
            2, 2, 2, 2
        )
        val expected = intArrayOf(
            0, 0, 0, 0,
            0, 0, 0, 0,
            2, 2, 2, 2,
            4, 4, 4, 4
        )

        swipeHandler.handleSwipeDown(input)
        assertArrayEquals(expected, input)
    }

    @Test
    fun `handleSwipeDown 2 2 0 2`() {
        val input = intArrayOf(
            2, 2, 2, 2,
            2, 2, 2, 2,
            0, 0, 0, 0,
            2, 2, 2, 2
        )
        val expected = intArrayOf(
            0, 0, 0, 0,
            0, 0, 0, 0,
            2, 2, 2, 2,
            4, 4, 4, 4
        )

        swipeHandler.handleSwipeDown(input)
        assertArrayEquals(expected, input)
    }

    @Test
    fun `handleSwipeDown 2 0 4 0`() {
        val input = intArrayOf(
            2, 2, 2, 2,
            0, 0, 0, 0,
            4, 4, 4, 4,
            0, 0, 0, 0
        )
        val expected = intArrayOf(
            0, 0, 0, 0,
            0, 0, 0, 0,
            2, 2, 2, 2,
            4, 4, 4, 4
        )

        swipeHandler.handleSwipeDown(input)
        assertArrayEquals(expected, input)
    }

    @Test
    fun `handleSwipeDown 2 4 0 4`() {
        val input = intArrayOf(
            2, 2, 2, 2,
            4, 4, 4, 4,
            0, 0, 0, 0,
            4, 4, 4, 4
        )
        val expected = intArrayOf(
            0, 0, 0, 0,
            0, 0, 0, 0,
            2, 2, 2, 2,
            8, 8, 8, 8
        )

        swipeHandler.handleSwipeDown(input)
        assertArrayEquals(expected, input)
    }

    // up
    @Test
    fun `handleSwipeUp no merger, only shifts`() {
        val input = intArrayOf(
            0, 0, 0, 2,
            0, 0, 2, 0,
            0, 2, 0, 0,
            2, 0, 0, 0
        )
        val expected = intArrayOf(
            2, 2, 2, 2,
            0, 0, 0, 0,
            0, 0, 0, 0,
            0, 0, 0, 0
        )

        swipeHandler.handleSwipeUp(input)
        assertArrayEquals(expected, input)
    }

    @Test
    fun `handleSwipeUp no merger, no shifts`() {
        val input = intArrayOf(
            2, 2, 2, 2,
            4, 4, 4, 4,
            2, 2, 2, 2,
            4, 4, 4, 4
        )
        val expected = intArrayOf(
            2, 2, 2, 2,
            4, 4, 4, 4,
            2, 2, 2, 2,
            4, 4, 4, 4
        )

        swipeHandler.handleSwipeUp(input)
        assertArrayEquals(expected, input)
    }

    @Test
    fun `handleSwipeUp one merger`() {
        val input = intArrayOf(
            0, 2, 0, 2,
            2, 2, 2, 0,
            2, 0, 2, 0,
            0, 0, 0, 2
        )
        val expected = intArrayOf(
            4, 4, 4, 4,
            0, 0, 0, 0,
            0, 0, 0, 0,
            0, 0, 0, 0
        )

        swipeHandler.handleSwipeUp(input)
        assertArrayEquals(expected, input)
    }

    @Test
    fun `handleSwipeUp two mergers`() {
        val input = intArrayOf(
            2, 2, 2, 2,
            2, 2, 2, 2,
            2, 2, 2, 2,
            2, 2, 2, 2
        )
        val expected = intArrayOf(
            4, 4, 4, 4,
            4, 4, 4, 4,
            0, 0, 0, 0,
            0, 0, 0, 0,
        )

        swipeHandler.handleSwipeUp(input)
        assertArrayEquals(expected, input)
    }

    @Test
    fun `handleSwipeUp three numbers and one merger`() {
        val input = intArrayOf(
            2, 2, 2, 2,
            2, 2, 2, 2,
            0, 0, 0, 0,
            2, 2, 2, 2
        )
        val expected = intArrayOf(
            4, 4, 4, 4,
            2, 2, 2, 2,
            0, 0, 0, 0,
            0, 0, 0, 0
        )

        swipeHandler.handleSwipeUp(input)
        assertArrayEquals(expected, input)
    }

    @Test
    fun `handleSwipeUp 2 2 0 2`() {
        val input = intArrayOf(
            2, 2, 2, 2,
            0, 0, 0, 0,
            2, 2, 2, 2,
            2, 2, 2, 2
        )
        val expected = intArrayOf(
            4, 4, 4, 4,
            2, 2, 2, 2,
            0, 0, 0, 0,
            0, 0, 0, 0
        )

        swipeHandler.handleSwipeUp(input)
        assertArrayEquals(expected, input)
    }

    @Test
    fun `handleSwipeUp 2 0 4 0`() {
        val input = intArrayOf(
            0, 0, 0, 0,
            4, 4, 4, 4,
            0, 0, 0, 0,
            2, 2, 2, 2
        )
        val expected = intArrayOf(
            4, 4, 4, 4,
            2, 2, 2, 2,
            0, 0, 0, 0,
            0, 0, 0, 0
        )

        swipeHandler.handleSwipeUp(input)
        assertArrayEquals(expected, input)
    }

    @Test
    fun `handleSwipeUp 2 4 0 4`() {
        val input = intArrayOf(
            4, 4, 4, 4,
            0, 0, 0, 0,
            4, 4, 4, 4,
            2, 2, 2, 2
        )
        val expected = intArrayOf(
            8, 8, 8, 8,
            2, 2, 2, 2,
            0, 0, 0, 0,
            0, 0, 0, 0
        )

        swipeHandler.handleSwipeUp(input)
        assertArrayEquals(expected, input)
    }
}