package ru.maksim.game2048

import ru.maksim.game2048.lib.data.GameBoardConstants
import ru.maksim.game2048.lib.data.preferences.Repository
import ru.maksim.game2048.lib.domain.GamePhase

suspend fun resetData(repository: Repository) {
    repository.saveBestTime(Long.MAX_VALUE)
    repository.saveGameBoard(GameBoardConstants.EMPTY_STRING)
    repository.saveGamePhase(GamePhase.NOT_STARTED)
    repository.saveTimeElapsed(0L)
}