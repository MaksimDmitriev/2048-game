package ru.maksim.game2048.presentation

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

internal class DirectionDetectorTest {

    @Test
    fun `getDirection from bottom to top`() {
        val x1 = 0.0f
        val x2 = -4.0f
        val y1 = 0.0f
        val y2 = -8.0f
        val res = DirectionDetector.getDirection(x1 = x1, x2 = x2, y1 = y1, y2 = y2)
        Assertions.assertEquals(Direction.UP, res)
    }

    @Test
    fun `getDirection from bottom to top but more to left`() {
        val x1 = 0.0f
        val x2 = -6.0f
        val y1 = 0.0f
        val y2 = 5.0f
        val res = DirectionDetector.getDirection(x1 = x1, x2 = x2, y1 = y1, y2 = y2)
        Assertions.assertEquals(Direction.LEFT, res)
    }

    @Test
    fun `getDirection from bottom to top but more to right`() {
        val x1 = 0.0f
        val x2 = 6.0f
        val y1 = 0.0f
        val y2 = 5.0f
        val res = DirectionDetector.getDirection(x1 = x1, x2 = x2, y1 = y1, y2 = y2)
        Assertions.assertEquals(Direction.RIGHT, res)
    }

    @Test
    fun `getDirection from top to bottom`() {
        val x1 = 0.0f
        val x2 = -1.0f
        val y1 = 0.0f
        val y2 = 8.0f
        val res = DirectionDetector.getDirection(x1 = x1, x2 = x2, y1 = y1, y2 = y2)
        Assertions.assertEquals(Direction.DOWN, res)
    }

    @Test
    fun `getDirection from top to bottom but more to left`() {
        val x1 = 0.0f
        val x2 = -9.0f
        val y1 = 0.0f
        val y2 = -8.0f
        val res = DirectionDetector.getDirection(x1 = x1, x2 = x2, y1 = y1, y2 = y2)
        Assertions.assertEquals(Direction.LEFT, res)
    }

    @Test
    fun `getDirection from top to bottom but more to right`() {
        val x1 = 0.0f
        val x2 = 6.0f
        val y1 = 0.0f
        val y2 = 2.0f
        val res = DirectionDetector.getDirection(x1 = x1, x2 = x2, y1 = y1, y2 = y2)
        Assertions.assertEquals(Direction.RIGHT, res)
    }
}