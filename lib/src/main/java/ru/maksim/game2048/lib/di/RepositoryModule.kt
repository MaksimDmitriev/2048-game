package ru.maksim.game2048.lib.di

import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import ru.maksim.game2048.lib.data.preferences.LocalDataSource
import ru.maksim.game2048.lib.data.preferences.PreferencesDataStoreImpl
import ru.maksim.game2048.lib.data.preferences.Repository
import ru.maksim.game2048.lib.data.preferences.RepositoryImpl
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
internal object RepositoryModule {

    @Provides
    @Singleton
    fun provideRepository(@ApplicationContext context: Context): Repository {
        val localDataSource = LocalDataSource(PreferencesDataStoreImpl(context))
        return RepositoryImpl(localDataSource)
    }
}