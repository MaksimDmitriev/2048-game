package ru.maksim.game2048.lib.data.preferences

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.preferencesDataStore
import dagger.hilt.android.qualifiers.ApplicationContext

private const val USER_PREFERENCES_NAME = "user_preferences"
private val Context.dataStore by preferencesDataStore(
    name = USER_PREFERENCES_NAME
)

interface PreferencesDataStore {

    val dataStore: DataStore<Preferences>
}

internal class PreferencesDataStoreImpl(@ApplicationContext private val context: Context) : PreferencesDataStore {

    override val dataStore: DataStore<Preferences>
        get() = context.dataStore
}