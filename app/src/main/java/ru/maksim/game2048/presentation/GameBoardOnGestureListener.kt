package ru.maksim.game2048.presentation

import android.view.GestureDetector
import android.view.MotionEvent
import androidx.annotation.VisibleForTesting
import ru.maksim.game2048.presentation.viewmodel.GamePlayViewModel
import kotlin.math.abs
import kotlin.math.atan

class GameBoardOnGestureListener(private val gamePlayViewModel: GamePlayViewModel) : GestureDetector.SimpleOnGestureListener() {

    override fun onFling(event1: MotionEvent, event2: MotionEvent, velocityX: Float, velocityY: Float): Boolean {
        val x1 = event1.x
        val y1 = event1.y
        val x2 = event2.x
        val y2 = event2.y
        val direction = DirectionDetector.getDirection(
            x1 = x1,
            x2 = x2,
            y1 = y1,
            y2 = y2
        )
        when (direction) {
            Direction.UP -> gamePlayViewModel.onSwipeUp()
            Direction.DOWN -> gamePlayViewModel.onSwipeDown()
            Direction.LEFT -> gamePlayViewModel.onSwipeLeft()
            Direction.RIGHT -> gamePlayViewModel.onSwipeRight()
        }
        return true
    }
}

// Inspired by https://stackoverflow.com/a/26387629
@VisibleForTesting
enum class Direction {
    UP, DOWN, LEFT, RIGHT
}

@VisibleForTesting
object DirectionDetector {

    private const val RIGHT_ANGLE_IN_DEGREES = 45.0f
    private const val STRAIGHT_ANGLE_IN_DEGREES = 180.0f

    fun getDirection(x1: Float, x2: Float, y1: Float, y2: Float): Direction {
        val isFromBottomToTop = y2 < y1
        val isFromLeftToRight = x2 > x1
        val verticalSide = abs(y2 - y1)
        val horizontalSide = abs(x2 - x1)

        val angleInDegrees = atan(horizontalSide / verticalSide) * STRAIGHT_ANGLE_IN_DEGREES / Math.PI
        val isLessThan45Degrees = angleInDegrees.compareTo(RIGHT_ANGLE_IN_DEGREES) < 0

        if (isLessThan45Degrees) {
            return if (isFromBottomToTop) {
                Direction.UP
            } else {
                Direction.DOWN
            }
        }

        return if (isFromLeftToRight) {
            Direction.RIGHT
        } else {
            Direction.LEFT
        }
    }
}