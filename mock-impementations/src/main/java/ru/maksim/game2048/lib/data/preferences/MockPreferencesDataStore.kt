package ru.maksim.game2048.lib.data.preferences

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences

class MockPreferencesDataStore : PreferencesDataStore {

    lateinit var dataStoreInstance: DataStore<Preferences>

    override val dataStore: DataStore<Preferences>
        get() = dataStoreInstance

}