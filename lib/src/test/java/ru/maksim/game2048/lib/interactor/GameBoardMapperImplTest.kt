package ru.maksim.game2048.lib.interactor

import org.junit.jupiter.api.Assertions.assertArrayEquals
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class GameBoardMapperImplTest {

    private val mapperImpl = GameBoardMapperImpl()

    @Test
    fun toGameBoard() {
        assertArrayEquals(intArrayOf(1, 2, 3), mapperImpl.toGameBoard("1,2,3"))
    }

    @Test
    fun toPreference() {
        assertEquals("4,5,6", mapperImpl.toPreference(intArrayOf(4, 5, 6)))
    }
}
