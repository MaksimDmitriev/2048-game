package ru.maksim.game2048.presentation

import androidx.lifecycle.Lifecycle
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.swipeLeft
import androidx.test.espresso.action.ViewActions.swipeRight
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import dagger.hilt.android.testing.BindValue
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.UninstallModules
import kotlinx.coroutines.runBlocking
import org.hamcrest.core.AllOf
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import ru.maksim.game2048.R
import ru.maksim.game2048.lib.data.preferences.Repository
import ru.maksim.game2048.lib.di.GameBoardValuesGeneratorModule
import ru.maksim.game2048.lib.interactor.random.GameBoardValuesGenerator
import ru.maksim.game2048.lib.interactor.random.MockGameBoardValuesGenerator
import ru.maksim.game2048.resetData
import javax.inject.Inject

@UninstallModules(GameBoardValuesGeneratorModule::class)
@HiltAndroidTest
class MainActivityGameOverTest {

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @BindValue
    @JvmField
    val gameBoardValuesGenerator: GameBoardValuesGenerator = MockGameBoardValuesGenerator()

    @Inject
    lateinit var repository: Repository

    @Before
    fun setUp() {
        hiltRule.inject()
        runBlocking {
            resetData(repository)
        }
    }

    @Test
    fun youHaveWonIsShown() {
        val localGameBoardValuesGenerator = gameBoardValuesGenerator as MockGameBoardValuesGenerator
        localGameBoardValuesGenerator.gameBoard = intArrayOf(
            0, 0, 0, 0,
            0, 0, 0, 0,
            0, 0, 0, 0,
            0, 0, 1024, 1024
        )

        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
            // Moves the activity state to State.RESUMED.
            scenario.moveToState(Lifecycle.State.RESUMED)

            onView(withId(R.id.fragment_container_game))
                .perform(swipeLeft())

            // TODO: I doubt the test is reliable 'cause some time is required to emit the new game phase value YOU_LOST and react to it
            onView(withId(R.id.game_over))
                .check(matches(AllOf(isDisplayed(), withText(R.string.you_won))))
        }
    }

    @Test
    fun youHaveLostIsShown() {
        val localGameBoardValuesGenerator = gameBoardValuesGenerator as MockGameBoardValuesGenerator
        localGameBoardValuesGenerator.gameBoard = intArrayOf(
            8, 16, 8, 16,
            16, 8, 16, 8,
            8, 16, 8, 16,
            8, 16, 8, 0
        )
        // Add a new value to the first column of the last row
        localGameBoardValuesGenerator.indexToAddNewValue = 12

        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
            // Moves the activity state to State.RESUMED.
            scenario.moveToState(Lifecycle.State.RESUMED)

            onView(withId(R.id.fragment_container_game))
                .perform(swipeRight())

            // TODO: I doubt the test is reliable 'cause some time is required to emit the new game phase value YOU_WON and react to it
            onView(withId(R.id.game_over))
                .check(matches(AllOf(isDisplayed(), withText(R.string.you_lost))))
        }
    }

    @After
    fun tearDown() {
        runBlocking {
            resetData(repository)
        }
    }
}
