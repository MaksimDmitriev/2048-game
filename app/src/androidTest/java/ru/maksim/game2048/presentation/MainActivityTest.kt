package ru.maksim.game2048.presentation

import android.view.View
import android.widget.TextView
import androidx.lifecycle.Lifecycle
import androidx.recyclerview.widget.RecyclerView
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.UiController
import androidx.test.espresso.ViewAction
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.isAssignableFrom
import dagger.hilt.android.testing.BindValue
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.UninstallModules
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import org.hamcrest.Matcher
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import ru.maksim.game2048.R
import ru.maksim.game2048.lib.data.GameBoardConstants
import ru.maksim.game2048.lib.data.preferences.Repository
import ru.maksim.game2048.lib.di.GameBoardValuesGeneratorModule
import ru.maksim.game2048.lib.domain.GamePhase
import ru.maksim.game2048.lib.interactor.random.GameBoardValuesGenerator
import ru.maksim.game2048.lib.interactor.random.MockGameBoardValuesGenerator
import ru.maksim.game2048.resetData
import javax.inject.Inject

@UninstallModules(GameBoardValuesGeneratorModule::class)
@HiltAndroidTest
class MainActivityTest {

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @BindValue
    @JvmField
    val gameBoardValuesGenerator: GameBoardValuesGenerator = MockGameBoardValuesGenerator()

    @Inject
    lateinit var repository: Repository

    @Before
    fun setUp() {
        hiltRule.inject()
        runBlocking {
            resetData(repository)
        }
        configureMockGameBoardValuesGenerator()
    }

    @Test
    fun generateNewData_press_Home_and_reopen() {
        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
            // Moves the activity state to State.RESUMED.
            scenario.moveToState(Lifecycle.State.RESUMED)

            val initialGameBoard = getGameBoardData()

            // close the screen
            scenario.moveToState(Lifecycle.State.STARTED)
            scenario.moveToState(Lifecycle.State.CREATED)

            // re-open the screen
            scenario.moveToState(Lifecycle.State.STARTED)
            scenario.moveToState(Lifecycle.State.RESUMED)

            val gameBoardAfterReopening = getGameBoardData()

            Assert.assertEquals(initialGameBoard, gameBoardAfterReopening)
        }
    }

    @Test
    fun generateNewData_swipeUp_and_verify_new_number_generated() {
        generateNewData_swipe_and_verify_new_number_generated(ViewActions.swipeUp())
    }

    @Test
    fun generateNewData_swipeLeft_and_verify_new_number_generated() {
        generateNewData_swipe_and_verify_new_number_generated(ViewActions.swipeLeft())
    }

    @Test
    fun generateNewData_swipeRight_and_verify_new_number_generated() {
        generateNewData_swipe_and_verify_new_number_generated(ViewActions.swipeRight())
    }

    @Test
    fun generateNewData_swipeDown_and_verify_new_number_generated() {
        generateNewData_swipe_and_verify_new_number_generated(ViewActions.swipeDown())
    }

    @After
    fun tearDown() {
        runBlocking {
            resetData(repository)
        }
    }

    private fun generateNewData_swipe_and_verify_new_number_generated(viewAction: ViewAction) {
        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
            // Moves the activity state to State.RESUMED.
            scenario.moveToState(Lifecycle.State.RESUMED)

            runBlocking {
                val gamePhaseBeforeSwipe = repository.observeGamePhase().first()
                Assert.assertEquals(GamePhase.NOT_STARTED, gamePhaseBeforeSwipe)
            }

            onView(ViewMatchers.withId(R.id.fragment_container_game))
                .perform(viewAction)

            val gameBoardAfterSwipe = getGameBoardData()
            val numberOfNonZeros = getNumberOfNonZeros(gameBoardAfterSwipe)
            Assert.assertEquals(3, numberOfNonZeros)

            // TODO: I suspect the test can be flaky because data writing is done on the IO thread under the hood.
            // the value might still be NOT_STARTED at this moment
            runBlocking {
                val gamePhaseBeforeSwipe = repository.observeGamePhase().first()
                Assert.assertEquals(GamePhase.IN_PROGRESS, gamePhaseBeforeSwipe)
            }
        }
    }

    private fun getNumberOfNonZeros(strings: List<String>): Int {
        var count = 0
        strings.forEach {
            if (it.isNotEmpty()) {
                count++
            }
        }
        return count
    }

    private fun configureMockGameBoardValuesGenerator() {
        val localGameBoardValuesGenerator = gameBoardValuesGenerator as MockGameBoardValuesGenerator
        localGameBoardValuesGenerator.gameBoard = intArrayOf(
            2, 0, 0, 0,
            0, 2, 0, 0,
            0, 0, 0, 0,
            0, 0, 0, 0
        )
        localGameBoardValuesGenerator.indexToAddNewValue = GameBoardConstants.GAME_BOARD_SIZE - 1
    }

    private fun getGameBoardData(): List<String> {
        val gameBoard = mutableListOf<String>()
        for (i in 0 until GameBoardConstants.GAME_BOARD_SIZE) {
            val getItemTextAction = GetItemTextAction()
            onView(ViewMatchers.withId(R.id.game_board))
                .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(i, getItemTextAction))
            gameBoard.add(getItemTextAction.text)
        }
        return gameBoard
    }
}

class GetItemTextAction : ViewAction {

    var text: String = GameBoardConstants.EMPTY_STRING

    override fun getConstraints(): Matcher<View> {
        return isAssignableFrom(TextView::class.java)
    }

    override fun getDescription(): String {
        return ""
    }

    override fun perform(uiController: UiController?, view: View?) {
        if (view == null) {
            return
        }
        text = (view as TextView).text.toString()
    }

}