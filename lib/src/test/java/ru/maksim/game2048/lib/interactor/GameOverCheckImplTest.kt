package ru.maksim.game2048.lib.interactor

import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import ru.maksim.game2048.lib.interactor.GameOverCheckImpl

internal class GameOverCheckImplTest {

    private val gameOverCheckImpl = GameOverCheckImpl()

    @Test
    fun hasWon() {
        val input = intArrayOf(
            0, 2, 2, 4,
            0, 2, 2, 4,
            0, 2, 2, 4,
            0, 2, 2, 2048,
        )
        assertTrue(gameOverCheckImpl.hasWon(input))
    }

    @Test
    fun `hasWon not yet`() {
        val input = intArrayOf(
            0, 2, 2, 4,
            0, 2, 1024, 4,
            0, 2, 2, 4,
            0, 2, 2, 16,
        )
        assertFalse(gameOverCheckImpl.hasWon(input))
    }

    @Test
    fun hasLost() {
        val input = intArrayOf(
            2, 4, 2, 4,
            4, 2, 4, 2,
            2, 4, 2, 4,
            4, 2, 4, 2
        )
        assertTrue(gameOverCheckImpl.hasLost(input))
    }

    @Test
    fun `hasLost next equal but not adjacent`() {
        val input = intArrayOf(
            2, 4, 2, 8,
            8, 2, 4, 2,
            2, 4, 2, 4,
            4, 2, 4, 2
        )
        assertTrue(gameOverCheckImpl.hasLost(input))
    }

    // There is no test for "element to left".
    // For example, if a[5] = 8 and a[6] = 8, we will check a[5] first and return true since a[6], which is equal to a[5] is
    // to the right of a[5].
    @Test
    fun `hasLost false because of element to left`() {
        val input = intArrayOf(
            2, 4, 2, 4,
            4, 8, 8, 2,
            2, 4, 2, 4,
            4, 2, 4, 2
        )
        assertFalse(gameOverCheckImpl.hasLost(input))
    }

    // There is no test for "element to top".
    // For example, if a[5] = 8 and a[9] = 8, we will check a[5] first and return true since a[9], which is equal to a[5] is
    // to the bottom of a[5].
    @Test
    fun `hasLost false because of element to bottom`() {
        val input = intArrayOf(
            2, 4, 2, 4,
            4, 8, 4, 2,
            2, 8, 2, 4,
            4, 2, 4, 2
        )
        assertFalse(gameOverCheckImpl.hasLost(input))
    }

    @Test
    fun `hasLost false because of first and second`() {
        val input = intArrayOf(
            8, 8, 2, 4,
            4, 2, 4, 2,
            2, 4, 2, 4,
            4, 2, 4, 2
        )
        assertFalse(gameOverCheckImpl.hasLost(input))
    }

    @Test
    fun `hasLost false because of first and to bottom`() {
        val input = intArrayOf(
            8, 4, 2, 4,
            8, 2, 4, 2,
            2, 4, 2, 4,
            4, 2, 4, 2
        )
        assertFalse(gameOverCheckImpl.hasLost(input))
    }

    @Test
    fun `hasLost false because of last and to right`() {
        val input = intArrayOf(
            2, 4, 2, 4,
            4, 2, 4, 2,
            2, 4, 2, 4,
            4, 2, 8, 8
        )
        assertFalse(gameOverCheckImpl.hasLost(input))
    }

    @Test
    fun `hasLost false because of last and to bottom`() {
        val input = intArrayOf(
            2, 4, 2, 4,
            4, 2, 4, 2,
            2, 4, 2, 8,
            4, 2, 4, 8
        )
        assertFalse(gameOverCheckImpl.hasLost(input))
    }

    @Test
    fun `hasLost false because there is at least one zero`() {
        val input = intArrayOf(
            16, 8, 16, 2,
            2, 64, 2, 64,
            0, 4, 128, 8,
            4, 0, 2, 4
        )
        assertFalse(gameOverCheckImpl.hasLost(input))
    }
}