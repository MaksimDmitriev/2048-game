package ru.maksim.game2048.presentation.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import ru.maksim.game2048.lib.data.GameBoardConstants
import ru.maksim.game2048.lib.data.preferences.Repository
import ru.maksim.game2048.lib.interactor.TimeFormatter
import javax.inject.Inject

@HiltViewModel
class BestTimeViewModel @Inject constructor(
    private val repository: Repository,
    private val timeFormatter: TimeFormatter
) : ViewModel() {

    private val _uiState = MutableStateFlow(GameBoardConstants.EMPTY_STRING)
    val uiState: StateFlow<String> = _uiState

    init {
        viewModelScope.launch {
            repository.observeBestTime()
                .filter { it != Long.MAX_VALUE }
                .map { timeFormatter.format(it) }
                .collect {
                    _uiState.value = it
                }
        }
    }
}