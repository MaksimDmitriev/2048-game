package ru.maksim.game2048.presentation.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import ru.maksim.game2048.lib.data.preferences.Repository
import ru.maksim.game2048.lib.domain.GamePhase
import ru.maksim.game2048.lib.domain.State
import ru.maksim.game2048.lib.interactor.GameBoardMapper
import ru.maksim.game2048.lib.interactor.GameOverCheck
import ru.maksim.game2048.lib.interactor.SwipeHandler
import ru.maksim.game2048.lib.interactor.random.GameBoardValuesGenerator
import javax.inject.Inject

@HiltViewModel
class GamePlayViewModel @Inject constructor(
    private val repository: Repository,
    private val gameBoardValuesGenerator: GameBoardValuesGenerator,
    private val gameBoardMapper: GameBoardMapper,
    private val swipeHandler: SwipeHandler,
    private val gameOverCheck: GameOverCheck
) : ViewModel() {

    private val _myUiState = MutableStateFlow(State())
    val myUiState: StateFlow<State> = _myUiState

    // Load data from a suspend fun and mutate state
    init {
        viewModelScope.launch {
            repository.observeGameBoard()
                .map {
                    if (it.isEmpty()) {
                        val newGameBoard = gameBoardValuesGenerator.generateNewGameBoard()
                        repository.saveGameBoard(gameBoardMapper.toPreference(newGameBoard))
                        repository.saveGamePhase(GamePhase.NOT_STARTED)
                        newGameBoard
                    } else {
                        gameBoardMapper.toGameBoard(it)
                    }
                }
                .onEach { gameBoard ->
                    val hasWon = gameOverCheck.hasWon(gameBoard)
                    if (hasWon) {
                        repository.saveGamePhase(GamePhase.WON)
                    } else if (gameOverCheck.hasLost(gameBoard)) {
                        repository.saveGamePhase(GamePhase.LOST)
                    }
                }
                .combine(repository.observeGamePhase()) { gameBoard, phase -> Pair(gameBoard, phase) }
                .collect { (gameBoard, phase) ->
                    _myUiState.value = State(gameBoard = gameBoard, gamePhase = phase)
                }
        }
    }

    fun onSwipeLeft() {
        val gameBoard = _myUiState.value.gameBoard
        onSwipe(gameBoard) { swipeHandler.handleSwipeLeft(gameBoard) }
    }

    fun onSwipeRight() {
        val gameBoard = _myUiState.value.gameBoard
        onSwipe(gameBoard) { swipeHandler.handleSwipeRight(gameBoard) }
    }

    fun onSwipeUp() {
        val gameBoard = _myUiState.value.gameBoard
        onSwipe(gameBoard) { swipeHandler.handleSwipeUp(gameBoard) }
    }

    fun onSwipeDown() {
        val gameBoard = _myUiState.value.gameBoard
        onSwipe(gameBoard) { swipeHandler.handleSwipeDown(gameBoard) }
    }

    fun onNewGameButtonClicked() {
        viewModelScope.launch {
            repository.saveGameBoard(gameBoardMapper.toPreference(gameBoardValuesGenerator.generateNewGameBoard()))
            repository.saveGamePhase(GamePhase.NOT_STARTED)
        }
    }

    private fun onSwipe(gameBoard: IntArray, swipeFunction: SwipeHandler.(input: IntArray) -> Unit) {
        swipeFunction.invoke(swipeHandler, gameBoard)
        gameBoardValuesGenerator.addValueToGameBoard(gameBoard)
        viewModelScope.launch {
            repository.saveGameBoard(gameBoardMapper.toPreference(gameBoard))
            val phase = repository.observeGamePhase().first()
            if (phase == GamePhase.NOT_STARTED) {
                repository.saveGamePhase(GamePhase.IN_PROGRESS)
            }
        }
    }
}
