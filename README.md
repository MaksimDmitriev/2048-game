Assemble the project, run lint and tests

```
./gradlew clean testDebugUnitTest lintDebug assembleDebug connectedDebugAndroidTest
```

https://youtube.com/shorts/098XvZkb1ao?feature=share