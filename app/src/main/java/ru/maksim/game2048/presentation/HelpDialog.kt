package ru.maksim.game2048.presentation

import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import ru.maksim.game2048.R

class HelpDialog : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return AlertDialog.Builder(requireContext())
            .setView(R.layout.dialog_help)
            .setPositiveButton(R.string.ok, null)
            .create()
    }

    companion object {

        fun newInstance() = HelpDialog()
    }
}