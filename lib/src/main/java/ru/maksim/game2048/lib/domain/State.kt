package ru.maksim.game2048.lib.domain

// TODO: I implemented it as a data class with hashCode and equals. Then it stopped getting states in
// GameFragment after the first state.
class State(val gameBoard: IntArray = intArrayOf(), @GamePhase val gamePhase: Int = GamePhase.NOT_STARTED) {

    override fun toString(): String {
        return "gameBoard=${gameBoard.contentToString()} gamePhase=$gamePhase"
    }
}