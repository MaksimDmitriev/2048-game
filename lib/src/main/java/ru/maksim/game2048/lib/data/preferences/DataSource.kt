package ru.maksim.game2048.lib.data.preferences

import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.emptyPreferences
import androidx.datastore.preferences.core.intPreferencesKey
import androidx.datastore.preferences.core.longPreferencesKey
import androidx.datastore.preferences.core.stringPreferencesKey
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import ru.maksim.game2048.lib.data.GameBoardConstants
import ru.maksim.game2048.lib.domain.GamePhase

interface DataSource {

    fun observeBestTime(): Flow<Long>

    fun observeGameBoard(): Flow<String>

    suspend fun saveBestTime(newBestTime: Long)

    suspend fun saveGameBoard(gameBoardData: String)

    suspend fun saveGamePhase(@GamePhase phase: Int)

    fun observeGamePhase(): Flow<Int>

    suspend fun saveTimeElapsed(timeElapsed: Long)

    fun observeTimeElapsed(): Flow<Long>

    companion object {
        val EMPTY_PREFERENCES = emptyPreferences()
    }
}

internal class LocalDataSource(private val preferencesDataStore: PreferencesDataStore) : DataSource {

    override fun observeBestTime(): Flow<Long> =
        preferencesDataStore.dataStore.data
            .catch {
                emit(DataSource.EMPTY_PREFERENCES)
            }
            .map { preferences ->
                preferences[PreferencesKeys.BEST_TIME] ?: Long.MAX_VALUE
            }

    override suspend fun saveBestTime(newBestTime: Long) {
        preferencesDataStore.dataStore.edit { preferences ->
            preferences[PreferencesKeys.BEST_TIME] = newBestTime
        }
    }

    override suspend fun saveGameBoard(gameBoardData: String) {
        preferencesDataStore.dataStore.edit { preferences ->
            preferences[PreferencesKeys.GAME_BOARD_DATA] = gameBoardData
        }
    }

    override suspend fun saveGamePhase(phase: Int) {
        preferencesDataStore.dataStore.edit { preferences ->
            preferences[PreferencesKeys.GAME_PHASE] = phase
        }
    }

    override fun observeGamePhase(): Flow<Int> =
        preferencesFlow()
            .map { preferences ->
                preferences[PreferencesKeys.GAME_PHASE] ?: GamePhase.NOT_STARTED
            }


    override fun observeGameBoard(): Flow<String> =
        preferencesFlow()
            .map { preferences ->
                preferences[PreferencesKeys.GAME_BOARD_DATA] ?: GameBoardConstants.EMPTY_STRING
            }

    override suspend fun saveTimeElapsed(timeElapsed: Long) {
        preferencesDataStore.dataStore.edit { preferences ->
            preferences[PreferencesKeys.TIME_ELAPSED] = timeElapsed
        }
    }

    override fun observeTimeElapsed(): Flow<Long> =
        preferencesFlow()
            .map { preferences ->
                preferences[PreferencesKeys.TIME_ELAPSED] ?: 0L
            }

    private fun preferencesFlow() = preferencesDataStore.dataStore.data
        .catch {
            emit(DataSource.EMPTY_PREFERENCES)
        }
}

internal object PreferencesKeys {
    val BEST_TIME = longPreferencesKey("BEST_TIME")
    val GAME_PHASE = intPreferencesKey("GAME_PHASE")
    val TIME_ELAPSED = longPreferencesKey("TIME_ELAPSED")
    val GAME_BOARD_DATA = stringPreferencesKey("GAME_BOARD_DATA")
}