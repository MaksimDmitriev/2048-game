package ru.maksim.game2048.presentation

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.launch
import ru.maksim.game2048.R
import ru.maksim.game2048.lib.data.AppConstants
import ru.maksim.game2048.presentation.viewmodel.BestTimeViewModel
import ru.maksim.game2048.presentation.viewmodel.GamePlayViewModel
import ru.maksim.game2048.presentation.viewmodel.TimerViewModel

@AndroidEntryPoint
class MetaDataFragment : Fragment(R.layout.fragment_meta_data) {

    private val bestTimerViewModel: BestTimeViewModel by activityViewModels()
    private val timerViewModel: TimerViewModel by activityViewModels()
    private val gamePlayViewModel: GamePlayViewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val bestTimeView = view.findViewById<TextView>(R.id.best_time_value)
        val timerView = view.findViewById<TextView>(R.id.timer_value)

        // Create a new coroutine in the lifecycleScope
        viewLifecycleOwner.lifecycleScope.launch {
            // repeatOnLifecycle launches the block in a new coroutine every time the
            // lifecycle is in the STARTED state (or above) and cancels it when it's STOPPED.
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                // Trigger the flow and start listening for values.
                // This happens when lifecycle is STARTED and stops
                // collecting when the lifecycle is STOPPED
                timerViewModel.observeTimer()
                    .collect {
                        timerView.text = it
                    }
            }
        }

        viewLifecycleOwner.lifecycleScope.launch {
            // repeatOnLifecycle launches the block in a new coroutine every time the
            // lifecycle is in the STARTED state (or above) and cancels it when it's STOPPED.
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                // Trigger the flow and start listening for values.
                // This happens when lifecycle is STARTED and stops
                // collecting when the lifecycle is STOPPED
                bestTimerViewModel.uiState
                    .filter { it.isNotEmpty() }
                    .collect {
                        bestTimeView.text = it
                    }
            }
        }

        view.findViewById<Button>(R.id.new_game_button).setOnClickListener {
            gamePlayViewModel.onNewGameButtonClicked()
        }

        lifecycle.addObserver(MetaDataFragmentLifecycleObserver(timerViewModel))
    }
}

private class MetaDataFragmentLifecycleObserver(private val timerViewModel: TimerViewModel) : DefaultLifecycleObserver {

    override fun onStop(owner: LifecycleOwner) {
        super.onStop(owner)
        timerViewModel.saveTimeElapsed()
    }
}