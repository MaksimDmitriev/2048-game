package ru.maksim.game2048.lib.interactor.random

import kotlin.random.Random

internal interface RandomNumberGenerator {

    fun nextInt(from: Int, until: Int): Int

    fun nextInt(until: Int): Int

    fun nextInt(): Int
}

internal class RandomNumberGeneratorImpl : RandomNumberGenerator {

    override fun nextInt(from: Int, until: Int) = Random.nextInt(from, until)

    override fun nextInt(until: Int) = Random.nextInt(until)

    override fun nextInt() = Random.nextInt()

}